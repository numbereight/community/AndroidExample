package ai.numbereight.test

import ai.numbereight.audiences.Audiences
import ai.numbereight.codesnippets.BuildConfig
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


class AudiencesTest {
    companion object {
        private const val LOG_TAG = "AudiencesExample"
    }

    @Test
    fun testAudiencesStart() {
        val audiencesCountDownLatch = CountDownLatch(1)
        val neCountDownLatch = CountDownLatch(1)

        val token = NumberEight.start(
            BuildConfig.REPLACE_WITH_DEVELOPER_KEY,
            InstrumentationRegistry.getInstrumentation().targetContext,
            ConsentOptions.withConsentToAll(),
            object: NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight started successfully.")
                    neCountDownLatch.countDown()
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight failed to start.", ex)
                    assertNull("An exception was returned by NumberEight.start()", ex)
                    neCountDownLatch.countDown()
                }
            }
        )

        Audiences.startRecording(token,
            object: NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight Audiences started successfully.")
                    audiencesCountDownLatch.countDown()
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight Audiences failed to start.", ex)
                    assertNull("An exception was returned by Audiences.startRecording()", ex)
                    audiencesCountDownLatch.countDown()
                }
            }
        )

        neCountDownLatch.await(5, TimeUnit.SECONDS)
        audiencesCountDownLatch.await(5, TimeUnit.SECONDS)

        if (neCountDownLatch.count != 0L) {
            assertTrue("NumberEight.start() timed out", false)
        }

        if (audiencesCountDownLatch.count != 0L) {
            assertTrue("Audiences.startRecording() timed out", false)
        }
    }
}