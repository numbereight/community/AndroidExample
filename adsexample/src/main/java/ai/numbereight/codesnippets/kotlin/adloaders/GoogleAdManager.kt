package ai.numbereight.codesnippets.kotlin.adloaders

import ai.numbereight.audiences.Audiences
import ai.numbereight.audiences.Liveness
import ai.numbereight.codesnippets.AdLoader
import android.app.Activity
import android.view.View
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerAdView
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import java.util.concurrent.CompletableFuture

class GoogleAdManager(private val activity: Activity, private val bannerView: AdManagerAdView) :
    AdLoader() {
    private val interstitialAdUnitId = "/22836750855/test-mobile-video-interstitial"
    private val bannerPromise = PromiseContainer<Boolean>()
    private val interstitialPromise = PromiseContainer<Boolean>()

    init {
        // Initialize the Google Mobile Ads SDK.
        MobileAds.initialize(activity)
        setupAdUnits();
    }

    private fun setupAdUnits() {
        val bannerListener: AdListener = object : AdListener() {
            override fun onAdLoaded() {
                if (bannerPromise.promise?.complete(true) == true) {
                    bannerView.visibility = View.VISIBLE
                }
            }

            override fun onAdFailedToLoad(error: LoadAdError) {
                bannerPromise.promise?.completeExceptionally(RuntimeException(error.message))
            }
        }
        bannerView.adListener = bannerListener
    }

    private fun generateRequest(): AdManagerAdRequest {
        /// Begin CodeSnippet: CollectMemberships
        // Collect audiences for all liveness types
        val memberships = Audiences.currentMemberships
        val habitual = memberships.filter { it.liveness == Liveness.HABITUAL }
        val live = memberships.filter { it.liveness == Liveness.LIVE }
        val today = memberships.filter { it.liveness == Liveness.HAPPENED_TODAY }
        val thisWeek = memberships.filter { it.liveness == Liveness.HAPPENED_THIS_WEEK }
        val thisMonth = memberships.filter { it.liveness == Liveness.HAPPENED_THIS_MONTH }
        /// End CodeSnippet: CollectMemberships

        /// Begin CodeSnippet: MakeGAMRequest
        // Create an ad request filled with NumberEight Audiences Taxonomy IDs
        // Audiences build up over time: live audiences are available after a few seconds, whereas
        // habitual ones can take several days.
        // While testing, it is recommended to continually make new ad requests to see as many
        // audiences as possible.
        val request = AdManagerAdRequest.Builder()
            .addCustomTargeting("ne_habitual", habitual.map { it.id })
            .addCustomTargeting("ne_live", live.map { it.id })
            .addCustomTargeting("ne_today", today.map { it.id })
            .addCustomTargeting("ne_this_week", thisWeek.map { it.id })
            .addCustomTargeting("ne_this_month", thisMonth.map { it.id })
            .build()
        /// End CodeSnippet: MakeGAMRequest

        return request
    }

    override fun loadBanner(): CompletableFuture<Boolean> {
        return wrapPromise(bannerPromise) {
            val request = generateRequest()
            /// Begin CodeSnippet: LoadGAMBannerAd
            bannerView.loadAd(request)
            /// End CodeSnippet: LoadGAMBannerAd
        }
    }

    override fun closeBanner() {
        bannerPromise.promise?.cancel(true)
        bannerView.visibility = View.INVISIBLE
    }

    override fun loadInterstitial(): CompletableFuture<Boolean> {
        return wrapPromise(interstitialPromise) { promise ->
            val request = generateRequest()
            InterstitialAd.load(
                activity, interstitialAdUnitId, request,
                object : InterstitialAdLoadCallback() {
                    override fun onAdLoaded(ad: InterstitialAd) {
                        if (promise.complete(true)) {
                            ad.show(activity)
                        }
                    }

                    override fun onAdFailedToLoad(error: LoadAdError) {
                        promise.completeExceptionally(RuntimeException(error.message))
                    }
                })
        }
    }

    override fun cancelInterstitial() {
        interstitialPromise.promise?.cancel(true)
    }
}