package ai.numbereight.codesnippets

import ai.numbereight.audiences.Audiences
import ai.numbereight.audiences.Liveness
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Button
import android.widget.TextView


class MainMenu : Activity() {
    companion object {
        private const val LOG_TAG = "AdsExample"
        private const val UPDATE_INTERVAL = 1000L
    }

    private lateinit var audiencesText: TextView
    private fun initUI() {
        this.audiencesText = findViewById(R.id.audiencesText)

        val adsBtn = findViewById<Button>(R.id.adsBtn)
        adsBtn.setOnClickListener {
            val adsActivity = Intent(this@MainMenu, AdsMenu::class.java)
            startActivity(adsActivity)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.main_menu)
        super.onCreate(savedInstanceState)
        initUI()
        initNumberEight()
    }

    private fun initNumberEight() {
        // Initialise the NumberEight SDK from your main Activity,
        // or the first Activity that starts in your app.
        val token: NumberEight.APIToken = NumberEight.start(
            BuildConfig.REPLACE_WITH_DEVELOPER_KEY,
            this,
            ConsentOptions.withConsentToAll(),
            object : NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight started successfully.")
                    runOnUiThread {
                        if (audiencesText.text == getString(R.string.audiences_placeholder)) {
                            audiencesText.text = getString(R.string.audiences_pending)
                        }
                    }
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight failed to start.", ex)
                    runOnUiThread {
                        audiencesText.text = ex.message
                    }
                }
            }
        )

        // Start Audiences
        Audiences.startRecording(token, object : NumberEight.OnStartListener {
            override fun onSuccess() {
                Log.i(LOG_TAG, "NumberEight Audiences started successfully.")
                runOnUiThread(this@MainMenu::displayAudiences)
            }

            override fun onFailure(ex: Exception) {
                Log.e(LOG_TAG, "NumberEight Audiences failed to start.", ex)
                runOnUiThread {
                    audiencesText.text = ex.message
                }
            }
        })
    }

    private fun displayAudiences() {
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed(object : Runnable {
            override fun run() {
                runOnUiThread {
                    val audiences: String = Audiences.currentMemberships
                        .filter { it.liveness == Liveness.LIVE || it.liveness == Liveness.HABITUAL }
                        .joinToString("\n") { it.name }
                    audiencesText.text = audiences
                }
                handler.postDelayed(this, UPDATE_INTERVAL)
            }
        }, UPDATE_INTERVAL)
    }
}