/// Begin CodeSnippet: SituationAndPlaceExample
package ai.numbereight.androidexample;

import android.util.Log;
import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;

import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.Parameters;
import ai.numbereight.sdk.types.event.Glimpse;
import ai.numbereight.sdk.types.NEPlace;
import ai.numbereight.sdk.types.NESituation;

public class MainJava extends Activity {
    private static final String LOG_TAG = "SituationAndPlace";
    private final NumberEight ne = new NumberEight();
    private NESituation currentSituation;
    private NEPlace currentPlace;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(),  new NumberEight.OnStartListener() {
            @Override
            public void onSuccess() {
                Log.i(LOG_TAG, "NumberEight started successfully.");
            }

            @Override
            public void onFailure(@NonNull Exception ex) {
                Log.e(LOG_TAG, "NumberEight failed to start.", ex);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i(LOG_TAG, "Waiting for situation and place updates...");
        ne.onSituationUpdated(
                Parameters.CHANGES_MOST_PROBABLE_ONLY,
                new NumberEight.SubscriptionCallback<NESituation>() {
                    @Override
                    public void onUpdated(@NonNull Glimpse<NESituation> glimpse) {
                        currentSituation = glimpse.getMostProbable();
                        process();
                    }
                }).onPlaceUpdated(
                Parameters.CHANGES_MOST_PROBABLE_ONLY,
                new NumberEight.SubscriptionCallback<NEPlace>() {
                    @Override
                    public void onUpdated(@NonNull Glimpse<NEPlace> glimpse) {
                        currentPlace = glimpse.getMostProbable();
                        process();
                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();
        ne.unsubscribeFromAll();
    }

    private void process() {
        if (currentSituation != null && currentPlace != null) {
            String text = currentSituation.toString() + " " + currentPlace.toString();
            Log.i(LOG_TAG, text);

            currentSituation = null;
            currentPlace = null;
        }
    }
}
/// End CodeSnippet: SituationAndPlaceExample
