/// Begin CodeSnippet: SituationAndPlaceExample
package ai.numbereight.androidexample

import android.util.Log
import android.app.Activity
import android.os.Bundle

import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.Parameters
import ai.numbereight.sdk.types.NEPlace
import ai.numbereight.sdk.types.NESituation

class MainKotlin : Activity() {
    companion object {
        private const val LOG_TAG = "SituationAndPlace"
    }

    private val ne = NumberEight()
    private var currentSituation: NESituation? = null
    private var currentPlace: NEPlace? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(), object: NumberEight.OnStartListener {
            override fun onSuccess() {
                Log.i(LOG_TAG, "NumberEight started successfully.")
            }

            override fun onFailure(ex: Exception) {
                Log.e(LOG_TAG, "NumberEight failed to start.", ex)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        Log.i(LOG_TAG, "Waiting for situation and place updates...")
        ne.onSituationUpdated(Parameters.CHANGES_MOST_PROBABLE_ONLY) { glimpse ->
            currentSituation = glimpse.mostProbable
            process()
        }.onPlaceUpdated(Parameters.CHANGES_MOST_PROBABLE_ONLY) { glimpse ->
            currentPlace = glimpse.mostProbable
            process()
        }
    }

    override fun onPause() {
        super.onPause()
        ne.unsubscribeFromAll()
    }

    private fun process() {
        if (currentSituation != null && currentPlace != null) {
            val text = currentSituation.toString() + " " + currentPlace.toString()
            Log.i(LOG_TAG, text)

            currentSituation = null
            currentPlace = null
        }
    }
}
/// End CodeSnippet: SituationAndPlaceExample
