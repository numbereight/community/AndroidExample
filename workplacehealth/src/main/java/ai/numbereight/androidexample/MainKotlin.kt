/// Begin CodeSnippet: WorkplaceHealthExample
package ai.numbereight.androidexample

import android.app.Activity
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import java.util.*
import kotlin.concurrent.schedule

import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.Parameters
import ai.numbereight.sdk.types.NEActivity
import ai.numbereight.sdk.types.NEPlace
import ai.numbereight.sdk.types.NEWeather

class MainKotlin : Activity() {
    companion object {
        private const val LOG_TAG = "WorkplaceHealth"
        private const val NOTIFICATION_ID = 0
        private const val CHANNEL_ID = "ai.numbereight.androidexample.channel"
    }

    private val ne = NumberEight()
    private var currentPlace: NEPlace? = null
    private var currentWeather: NEWeather? = null

    private var isSitting = false
    private var activityStart = 0L
    private var inactivityStart = 0L
    private val inactiveThreshold = 30 * 60 * 1_000L // 30 minutes
    private val changeThreshold = 5 * 60 * 1_000L // 5 minutes

    private var activeNotification: Notification? = null
    private lateinit var notificationManager: NotificationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(), object: NumberEight.OnStartListener {
            override fun onSuccess() {
                Log.i(LOG_TAG, "NumberEight started successfully.")
            }

            override fun onFailure(ex: Exception) {
                Log.e(LOG_TAG, "NumberEight failed to start.", ex)
            }
        })

        // Set up notification
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, "AndroidExample Notifications",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onResume() {
        super.onResume()

        ne.onActivityUpdated(Parameters.SENSITIVITY_SMOOTH) { glimpse ->
            synchronized(this@MainKotlin) {
                val state = glimpse.mostProbable.state

                if (state == NEActivity.State.Unknown) {
                    return@synchronized
                }

                isSitting = state == NEActivity.State.Stationary
                        || state == NEActivity.State.InVehicle
            }
        }.onPlaceUpdated { glimpse ->
            synchronized(this@MainKotlin) {
                currentPlace = glimpse.mostProbable
            }
        }.onWeatherUpdated { glimpse ->
            synchronized(this@MainKotlin) {
                currentWeather = glimpse.mostProbable
            }
        }

        // Run process() on a timer loop every minute
        Timer().schedule(0, 60 * 1_000L) {
            process()
        }
    }

    override fun onPause() {
        super.onPause()
        ne.unsubscribeFromAll()
    }

    @Synchronized
    private fun process() {
        val now = System.currentTimeMillis()

        if (isSitting && inactivityStart == 0L) {
            // Mark the start of inactivity
            inactivityStart = now
            Log.i(LOG_TAG, "Now inactive")
        } else if (!isSitting && activityStart == 0L) {
            // Mark the start of activity
            activityStart = now
            Log.i(LOG_TAG, "Now active")
        }

        val timeActive = now - activityStart
        val timeInactive = now - inactivityStart

        if (isSitting && timeInactive > changeThreshold) {
            // Mark the end of activity if inactive for longer than the change threshold
            activityStart = 0
        } else if (!isSitting && timeActive > changeThreshold) {
            // Mark the end of inactivity if active for longer than the change threshold
            inactivityStart = 0
            // Reset the notification
            if (activeNotification != null) {
                notificationManager.cancel(NOTIFICATION_ID)
                activeNotification = null
            }
        }

        // Check if the user has been inactive for too long and they
        // are at work.
        if (isSitting &&
                timeInactive > inactiveThreshold
                && currentPlace?.context?.work ==
                    NEPlace.Context.Knowledge.AtPlaceContext
                && activeNotification == null) {

            // Generate a suggestion based on weather
            val weather = currentWeather
            var text = "You've been sat at your desk for a while now - "
            if (weather != null) {
                if (weather.conditions == NEWeather.Conditions.Sunny
                        && weather.temperature >= NEWeather.Temperature.Warm) {
                    text += "it's nice outside, how about a walk?"
                } else {
                    text += "maybe walk around the office for a while?"
                }
            } else {
                text += "perhaps walk around for a bit?"
            }

            // Display the text as a notification
            activeNotification = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Notification.Builder(this, CHANNEL_ID)
                        .setContentText(text)
                        .build()
            } else {
                @Suppress("DEPRECATION")
                Notification.Builder(this)
                        .setContentText(text)
                        .notification
            }

            notificationManager.notify(NOTIFICATION_ID, activeNotification)
        }
    }
}
/// End CodeSnippet: WorkplaceHealthExample
