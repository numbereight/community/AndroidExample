package ai.numbereight.codesnippets.kotlin

import android.util.Log

import ai.numbereight.insights.Insights
import ai.numbereight.insights.RecordingConfig
import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.Parameters

class InsightsMarkerSnippets {
    companion object {
        private const val LOG_TAG = "InsightsExample"
    }

fun useDeviceId(token: NumberEight.APIToken) {

/// Begin CodeSnippet: InsightsSpecifyDeviceID
val config = RecordingConfig()
val deviceId = "insert_custom_device_or_user_id_here_if_required"
config.deviceId = deviceId
Insights.startRecording(token, config, object : NumberEight.OnStartListener {
    override fun onSuccess() {
        Log.i(LOG_TAG, "NumberEight Insights started successfully.")
    }

    override fun onFailure(ex: Exception) {
        Log.e(LOG_TAG, "NumberEight Insights failed to start.", ex)
    }
})
/// End CodeSnippet: InsightsSpecifyDeviceID
}


fun addContexts(token: NumberEight.APIToken) {

/// Begin CodeSnippet: InsightsSpecifyConfig
val config = RecordingConfig()

// Add the Device Movement context to the list of recorded topics
config.topics += NumberEight.kNETopicDeviceMovement

// Put a smoothing filter on the Device Movement context
config.filters +=
    NumberEight.kNETopicDeviceMovement to
        Parameters.SENSITIVITY_SMOOTHER.and(Parameters.CHANGES_ONLY).filter

Insights.startRecording(token, config, object : NumberEight.OnStartListener {
    override fun onSuccess() {
        Log.i(LOG_TAG, "NumberEight Insights started successfully.")
    }

    override fun onFailure(ex: Exception) {
        Log.e(LOG_TAG, "NumberEight Insights failed to start.", ex)
    }
})
/// End CodeSnippet: InsightsSpecifyConfig

}
}
