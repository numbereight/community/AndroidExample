package ai.numbereight.codesnippets.kotlin

import ai.numbereight.codesnippets.BuildConfig
import ai.numbereight.insights.Insights
import ai.numbereight.insights.RecordingConfig
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import android.util.Log
import android.app.Activity
import android.os.Bundle


/// Begin CodeSnippet: InitializeInsights
class MyInsightsActivity : Activity() {
    companion object {
        private const val LOG_TAG = "InsightsExample"
    }

    private fun initNumberEight() {
        // Initialise the NumberEight SDK from your main Activity,
        // or the first Activity that starts in your app.
        val token = NumberEight.start(
            BuildConfig.REPLACE_WITH_DEVELOPER_KEY,
            this,
            ConsentOptions.withConsentToAll(),
            object: NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight started successfully.")
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight failed to start.", ex)
                }
            }
        )

        // Start Insights
        Insights.startRecording(token, RecordingConfig(), object : NumberEight.OnStartListener {
            override fun onSuccess() {
                Log.i(LOG_TAG, "NumberEight Insights started successfully.")
            }

            override fun onFailure(ex: Exception) {
                Log.e(LOG_TAG, "NumberEight Insights failed to start.", ex)
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initNumberEight()
    }
}
/// End CodeSnippet: InitializeInsights
