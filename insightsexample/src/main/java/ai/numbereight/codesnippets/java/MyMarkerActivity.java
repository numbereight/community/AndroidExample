package ai.numbereight.codesnippets.java;

import android.app.Activity;
import android.os.Bundle;

import ai.numbereight.insights.Insights;

/// Begin CodeSnippet: InsightsMarkerEvents
public class MyMarkerActivity extends Activity {
    @Override
    public void onResume() {
        super.onResume();
        Insights.addMarker("screen_viewed");
    }

    public void purchaseMade(Integer value) {
        Bundle params = new Bundle();
        params.putInt("value", value);

        Insights.addMarker("in_app_purchase", params);
    }

    public void songPlayed(String title, String artist, String genre) {
        Bundle params = new Bundle();
        params.putString("title", title);
        params.putString("artist", artist);
        params.putString("genre", genre);

        Insights.addMarker("song_played", params);
    }
}
/// End CodeSnippet: InsightsMarkerEvents

