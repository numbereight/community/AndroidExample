package ai.numbereight.codesnippets.java;

import android.app.Activity;
import android.os.Bundle;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ai.numbereight.codesnippets.BuildConfig;
import ai.numbereight.insights.Insights;
import ai.numbereight.insights.RecordingConfig;
import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;

/// Begin CodeSnippet: InitializeInsights
public class MyInsightsActivity extends Activity {
    private static final String LOG_TAG = "InsightsExample";

    private void initNumberEight() {
        // Initialise the NumberEight SDK from your main Activity,
        // or the first Activity that starts in your app.
        NumberEight.APIToken token = NumberEight.start(
                BuildConfig.REPLACE_WITH_DEVELOPER_KEY,
                this,
                ConsentOptions.withConsentToAll(),
                new NumberEight.OnStartListener() {
                    @Override
                    public void onSuccess() {
                        Log.i(LOG_TAG, "NumberEight started successfully.");
                    }

                    @Override
                    public void onFailure(@NonNull Exception ex) {
                        Log.e(LOG_TAG, "NumberEight failed to start.", ex);
                    }
                }
        );

        // Start Insights
        Insights.startRecording(token, new RecordingConfig(), new NumberEight.OnStartListener() {
            @Override
            public void onSuccess() {
                Log.i(LOG_TAG, "NumberEight Insights started successfully.");
            }

            @Override
            public void onFailure(@NonNull Exception ex) {
                Log.e(LOG_TAG, "NumberEight Insights failed to start.", ex);
            }
        });
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initNumberEight();
    }
}
/// End CodeSnippet: InitializeInsights
