/// Begin CodeSnippet: GPSLocationExampleKotlin
package ai.numbereight.androidexample

import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.Parameters
import ai.numbereight.sdk.common.authorization.AuthorizationChallengeHandler
import ai.numbereight.sdk.types.NELocation
import ai.numbereight.sdk.types.event.Glimpse
import android.content.Context
import android.util.Log
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

class GPSLocationKotlin : AppCompatActivity() {
    companion object {
        private const val LOG_TAG = "LocationExample"
        const val PERMISSIONS_REQUEST = 1
    }

    private val ne = NumberEight()

    private var pendingAuthChallenge: AuthorizationChallengeHandler.Resolver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(),
            object: AuthorizationChallengeHandler {
                override fun requestAuthorization(context: Context, permissions: Array<String>, resolver: AuthorizationChallengeHandler.Resolver) {
                    pendingAuthChallenge = resolver

                    ActivityCompat.requestPermissions(this@GPSLocationKotlin,
                        permissions,
                        PERMISSIONS_REQUEST)
                }
            }, object: NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight started successfully.")
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight failed to start.", ex)
                }
            })
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_REQUEST -> {
                pendingAuthChallenge?.resolve()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        ne.subscribeTo(NumberEight.kNETopicGPSLocation, Parameters.SENSITIVITY_REAL_TIME) { glimpse: Glimpse<NELocation> ->
            val mostProbableLocation = glimpse.mostProbable.coordinate
            Log.i("GPS Point", "Got Location Update: ${mostProbableLocation.latitude}, ${mostProbableLocation.longitude}")
        }
        ne.subscribeTo(NumberEight.kNETopicLowPowerLocation, Parameters.SENSITIVITY_REAL_TIME) { glimpse: Glimpse<NELocation> ->
            val mostProbableLocation = glimpse.mostProbable.coordinate
            Log.i("GPS Point", "Got Low Power Location Update: ${mostProbableLocation.latitude}, ${mostProbableLocation.longitude}")
        }
    }

    override fun onPause() {
        super.onPause()
        ne.unsubscribeFromAll()
    }
}
/// End CodeSnippet: GPSLocationExampleKotlin
