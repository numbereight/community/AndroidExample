# The app builds fine on the phone, but fails in tests with a missing symbol
# from what we could see this is an open issue that has not been resolved yet
# Jul 28, 2023
-keep public class kotlin.LazyKt