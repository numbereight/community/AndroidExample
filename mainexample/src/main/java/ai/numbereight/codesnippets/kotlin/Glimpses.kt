package ai.numbereight.codesnippets.kotlin

import ai.numbereight.sdk.NumberEight
import android.app.Activity
import android.util.Log

/// Begin CodeSnippet: UpdatingGlimpses
class Glimpses: Activity() {
    // Declare a member variable to store a NumberEight subscription
    private val ne = NumberEight()

    override fun onResume() {
        super.onResume()

        // Register a new subscription for Device Position
        ne.onDevicePositionUpdated { glimpse ->
            Log.i("MyApp", "Device Position updated: $glimpse")
        }
    }

    override fun onPause() {
        super.onPause()

        // Cancel the subscription once finished
        ne.unsubscribeFromDevicePosition()
    }
}
/// End CodeSnippet: UpdatingGlimpses
