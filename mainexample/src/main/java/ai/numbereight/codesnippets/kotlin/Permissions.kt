package ai.numbereight.codesnippets.kotlin

import ai.numbereight.codesnippets.BuildConfig
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.common.authorization.AuthorizationChallengeHandler
import android.app.Activity
import android.util.Log
import android.content.Context
import android.os.Bundle
import androidx.core.app.ActivityCompat

class Permissions : Activity() {

    companion object {
        private const val LOG_TAG = "MainExample"
    }

override fun onCreate(savedInstanceState: Bundle?) {
super.onCreate(savedInstanceState)

/// Begin CodeSnippet: NumberEightGettingStartedAuthorization
NumberEight.start(
    BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(),
    object: AuthorizationChallengeHandler {
        override fun requestAuthorization(context: Context, permissions: Array<String>, resolver: AuthorizationChallengeHandler.Resolver) {
            // See https://developer.android.com/training/permissions/requesting
            // for more information.
            ActivityCompat.requestPermissions(
                this@Permissions,
                permissions,
                0)
            // ...
            resolver.resolve() // Call once user has granted/denied the permissions.
        }
    }, object: NumberEight.OnStartListener {
        override fun onSuccess() {
            Log.i(LOG_TAG, "NumberEight started successfully.")
        }

        override fun onFailure(ex: Exception) {
            Log.e(LOG_TAG, "NumberEight failed to start.", ex)
        }
    })
/// End CodeSnippet: NumberEightGettingStartedAuthorization

}
}
