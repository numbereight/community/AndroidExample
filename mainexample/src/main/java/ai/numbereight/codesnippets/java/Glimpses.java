package ai.numbereight.codesnippets.java;

import android.app.Activity;
import android.util.Log;

import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.types.NEDevicePosition;
import ai.numbereight.sdk.types.event.Glimpse;

/// Begin CodeSnippet: UpdatingGlimpses
public class Glimpses extends Activity {
    // Declare a member variable to store a NumberEight subscription
    private final NumberEight ne = new NumberEight();
    private final static String LOG_TAG = "MainExample";

    @Override
    public void onResume() {
        super.onResume();

        // Register a new subscription for Device Position
        ne.onDevicePositionUpdated(new NumberEight.SubscriptionCallback<NEDevicePosition>() {
            @Override
            public void onUpdated(Glimpse<NEDevicePosition> glimpse) {
                Log.i(LOG_TAG, "Device position updated: " + glimpse.toString());
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();

        // Cancel the subscription once finished
        ne.unsubscribeFromDevicePosition();
    }
}
/// End CodeSnippet: UpdatingGlimpses
