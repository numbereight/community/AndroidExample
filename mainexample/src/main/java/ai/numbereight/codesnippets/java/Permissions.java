package ai.numbereight.codesnippets.java;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import org.jetbrains.annotations.NotNull;

import ai.numbereight.codesnippets.BuildConfig;
import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.common.authorization.AuthorizationChallengeHandler;

class Permissions extends Activity {
    private static final String LOG_TAG = "MainExample";

@Override
public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);

/// Begin CodeSnippet: NumberEightGettingStartedAuthorization
NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, ConsentOptions.withConsentToAll(),
    new AuthorizationChallengeHandler() {
        @Override
        public void requestAuthorization(@NotNull Context context,
                                         @NotNull String[] permissions,
                                         @NotNull Resolver resolver) {
          // See https://developer.android.com/training/permissions/requesting
          // for more information.
          ActivityCompat.requestPermissions(Permissions.this,
              permissions,
              0); // Choose a suitable request code here.
          // ...
          resolver.resolve(); // Call once user has granted/denied the permissions.
        }
      }, new NumberEight.OnStartListener() {
        @Override
        public void onSuccess() {
            Log.i(LOG_TAG, "NumberEight started successfully.");
        }

        @Override
        public void onFailure(@NonNull Exception ex) {
            Log.e(LOG_TAG, "NumberEight failed to start.", ex);
        }
    });
/// End CodeSnippet: NumberEightGettingStartedAuthorization
}
}
