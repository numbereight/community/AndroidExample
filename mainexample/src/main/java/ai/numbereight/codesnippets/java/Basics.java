package ai.numbereight.codesnippets.java;

import android.app.Activity;

import android.util.Log;
import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.numbereight.codesnippets.BuildConfig;
import ai.numbereight.sdk.ConsentOptions;
import ai.numbereight.sdk.NumberEight;
import ai.numbereight.sdk.Parameters;
import ai.numbereight.sdk.common.snapshots.IFormatter;
import ai.numbereight.sdk.common.snapshots.ITranslator;
import ai.numbereight.sdk.common.snapshots.JSONFormatter;
import ai.numbereight.sdk.common.snapshots.QueryStringFormatter;
import ai.numbereight.sdk.common.snapshots.Snapshotter;
import ai.numbereight.sdk.types.ConsentOptionsProperty;
import ai.numbereight.sdk.types.NEActivity;
import ai.numbereight.sdk.types.NEDevicePosition;
import ai.numbereight.sdk.types.NEIndoorOutdoor;
import ai.numbereight.sdk.types.NELockStatus;
import ai.numbereight.sdk.types.NEMovement;
import ai.numbereight.sdk.types.NEPlace;
import ai.numbereight.sdk.types.NESituation;
import ai.numbereight.sdk.types.NETime;
import ai.numbereight.sdk.types.NEType;
import ai.numbereight.sdk.types.NEWeather;
import ai.numbereight.sdk.types.event.Glimpse;
import kotlin.Pair;

class Basics extends Activity {

    private static final String LOG_TAG = "MainExample";

    public void manageConsent() {
/// Begin CodeSnippet: ManageConsent
ConsentOptions consent = new ConsentOptions();
consent.setConsent(ConsentOptionsProperty.ALLOW_PROCESSING, true);
consent.setConsent(ConsentOptionsProperty.ALLOW_STORAGE, true);
consent.setConsent(ConsentOptionsProperty.ALLOW_PRECISE_GEOLOCATION, true);
consent.setConsent(ConsentOptionsProperty.ALLOW_USE_FOR_PERSONALISED_CONTENT, true);

NumberEight.start(BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, consent, new NumberEight.OnStartListener() {
    @Override
    public void onSuccess() {
        Log.i(LOG_TAG, "NumberEight started successfully.");
    }

    @Override
    public void onFailure(@NonNull Exception ex) {
        Log.e(LOG_TAG, "NumberEight failed to start.", ex);
    }
});

// You can also update consent at any time
NumberEight.setConsentOptions(consent);
/// End CodeSnippet: ManageConsent
    }

    public void functions() {
/// Begin CodeSnippet: OtherGlimpses
NumberEight ne = new NumberEight();

// Activity
ne.onActivityUpdated(new NumberEight.SubscriptionCallback<NEActivity>() {
    public void onUpdated(Glimpse<NEActivity> glimpse) { }
});
// Device Movement
ne.onDeviceMovementUpdated(new NumberEight.SubscriptionCallback<NEMovement>() {
    public void onUpdated(Glimpse<NEMovement> glimpse) { }
});
// Device Position
ne.onDevicePositionUpdated(new NumberEight.SubscriptionCallback<NEDevicePosition>() {
    public void onUpdated(Glimpse<NEDevicePosition> glimpse) { }
});
// Indoor/Outdoor
ne.onIndoorOutdoorUpdated(new NumberEight.SubscriptionCallback<NEIndoorOutdoor>() {
    public void onUpdated(Glimpse<NEIndoorOutdoor> glimpse) { }
});
// Place
ne.onPlaceUpdated(new NumberEight.SubscriptionCallback<NEPlace>() {
    public void onUpdated(Glimpse<NEPlace> glimpse) { }
});
// Situation
ne.onSituationUpdated(new NumberEight.SubscriptionCallback<NESituation>() {
    public void onUpdated(Glimpse<NESituation> glimpse) { }
});
// Time
ne.onTimeUpdated(new NumberEight.SubscriptionCallback<NETime>() {
    public void onUpdated(Glimpse<NETime> glimpse) { }
});
// Weather
ne.onWeatherUpdated(new NumberEight.SubscriptionCallback<NEWeather>() {
    public void onUpdated(Glimpse<NEWeather> glimpse) { }
});

// All other unnamed Glimpses (refer to the API reference for a full list of topics)
ne.subscribeTo(NumberEight.kNETopicLockStatus, new NumberEight.SubscriptionCallback<NELockStatus>() {
    public void onUpdated(Glimpse<NELockStatus> glimpse) { }
});
ne.subscribeTo("", new NumberEight.SubscriptionCallback<NEType>() {
    public void onUpdated(Glimpse<NEType> glimpse) { }
});
/// End CodeSnippet: OtherGlimpses
    }

    public void usingGlimpses() {
/// Begin CodeSnippet: UsingGlimpses
NumberEight ne = new NumberEight();

ne.onSituationUpdated(new NumberEight.SubscriptionCallback<NESituation>() {
    public void onUpdated(final Glimpse<NESituation> glimpse) {
    runOnUiThread(new Runnable() {
            @Override
            public void run() {
                 NESituation situation = glimpse.getMostProbable();

                 if (situation.getMajor() == NESituation.Major.Working
                         && situation.getMinor() == NESituation.Minor.InAnOffice) {
                     Log.d(LOG_TAG, "User is working in an office!");
                 }
            }
        });
    }
});
/// End CodeSnippet: UsingGlimpses
    }

    public void filteringGlimpses() {
/// Begin CodeSnippet: ParameterizedGlimpses
NumberEight ne = new NumberEight();

ne.onActivityUpdated(
    Parameters.CHANGES_MOST_PROBABLE_ONLY,
    new NumberEight.SubscriptionCallback<NEActivity>() {
        public void onUpdated(Glimpse<NEActivity> glimpse) {
            // Only called when the most probable activity has changed
        }
    });

ne.onActivityUpdated(
    Parameters.SENSITIVITY_SMOOTH,
    new NumberEight.SubscriptionCallback<NEActivity>() {
        public void onUpdated(Glimpse<NEActivity> glimpse) {
            // Updates are smoothed such that abrupt changes do not trigger the callback
        }
    });

ne.onActivityUpdated(
    Parameters.SENSITIVITY_LONG_TERM.and(Parameters.SIGNIFICANT_CHANGE),
    new NumberEight.SubscriptionCallback<NEActivity>() {
        public void onUpdated(Glimpse<NEActivity> glimpse) {
            // Only activities that have lasted for several minutes and whose
            // confidences have changed significantly from the last glimpse will
            // trigger the callback.
        }
	});
/// End CodeSnippet: ParameterizedGlimpses
    }

  public void multipleSubscriptions() {
/// Begin CodeSnippet: MultipleSubscriptions
NumberEight manager1 = new NumberEight();
NumberEight manager2 = new NumberEight();

manager1.onSituationUpdated(new NumberEight.SubscriptionCallback<NESituation>() {
  @Override
  public void onUpdated(Glimpse<NESituation> glimpse) {
      // Situation subscription #1
  }
}).onSituationUpdated(new NumberEight.SubscriptionCallback<NESituation>() {
  @Override
  public void onUpdated(Glimpse<NESituation> glimpse) {
      // Situation subscription #2
  }
});

manager2.onActivityUpdated(new NumberEight.SubscriptionCallback<NEActivity>() {
  @Override
  public void onUpdated(Glimpse<NEActivity> glimpse) {
      // Activity subscription #1
  }
}).onSituationUpdated(new NumberEight.SubscriptionCallback<NESituation>() {
  @Override
  public void onUpdated(Glimpse<NESituation> glimpse) {
      // Situation subscription #2
  }
});

manager1.unsubscribeFromAll();
manager2.unsubscribeFromSituation();
/// End CodeSnippet: MultipleSubscriptions
  }

public void snapshotter() {
/// Begin CodeSnippet: CreateDefaultSnapshotter
NumberEight.makeSnapshotter(Snapshotter.Companion.getDefaultFilters());
/// End CodeSnippet: CreateDefaultSnapshotter

/// Begin CodeSnippet: GetDefaultSnapshotterFilters
Snapshotter.Companion.getDefaultTopics();
Snapshotter.Companion.getDefaultFilters();
/// End CodeSnippet: GetDefaultSnapshotterFilters

/// Begin CodeSnippet: CreateCustomSnapshotter
List<String> topics = new ArrayList<>();
topics.add(NumberEight.kNETopicActivity);
topics.add(NumberEight.kNETopicPlace);
topics.add(NumberEight.kNETopicReachability);
NumberEight.makeSnapshotter(topics);

Map<String, Parameters> parameters = new HashMap<>();
parameters.put(NumberEight.kNETopicActivity, Parameters.CHANGES_ONLY);
parameters.put(NumberEight.kNETopicPlace, Parameters.CHANGES_ONLY);
parameters.put(NumberEight.kNETopicReachability, Parameters.CHANGES_ONLY);
NumberEight.makeSnapshotter(parameters);
/// End CodeSnippet: CreateCustomSnapshotter

Snapshotter snapshotter = NumberEight.makeSnapshotter(Snapshotter.Companion.getDefaultFilters());
/// Begin CodeSnippet: TakeSnapshot
Snapshotter.Snapshot<String> snapshot = snapshotter.takeSnapshot();

Snapshotter.Snapshot<String> customSnapshot = snapshotter.takeSnapshot(new ITranslator<String>() {
    @NonNull
    @Override
    public Pair<String, String> translate(@NonNull String originalTopic, @NonNull Glimpse<NEType> glimpse) {
        return new Pair<>(originalTopic, "value:" + glimpse.getMostProbable().toString() + " confidence:" + glimpse.getMostProbableConfidence());
    }
});
/// End CodeSnippet: TakeSnapshot

/// Begin CodeSnippet: PauseResumeSnapshotter
snapshotter.pause();
snapshotter.resume();
/// End CodeSnippet: PauseResumeSnapshotter

/// Begin CodeSnippet: FormatSnapshot
Log.d(LOG_TAG, snapshot.toString());

Log.d(LOG_TAG, snapshot.toString(new IFormatter<String>() {
    @NonNull
    @Override
    public String format(@NonNull Map<String, ? extends String> state) {
        return state.keySet().toString();
    }
}));
/// End CodeSnippet: FormatSnapshot

/// Begin CodeSnippet: UsingBuiltInFormatter
Log.d(LOG_TAG, snapshot.toString(new JSONFormatter<String>()));

Log.d(LOG_TAG, snapshot.toString(new QueryStringFormatter<String>()));
/// End CodeSnippet: UsingBuiltInFormatter
  }
}
