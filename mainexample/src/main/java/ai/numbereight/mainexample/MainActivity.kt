package ai.numbereight.mainexample

import ai.numbereight.codesnippets.BuildConfig
import ai.numbereight.codesnippets.R
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.common.authorization.AuthorizationChallengeHandler
import ai.numbereight.sdk.types.NEActivity
import android.content.Context
import android.util.Log
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

class MainActivity : AppCompatActivity() {
    companion object {
        private const val LOG_TAG = "MainExample"
        private const val REQUEST_NE_PERMISSIONS = 1
    }

    private var pendingAuthChallenge: AuthorizationChallengeHandler.Resolver? = null
    private val ne = NumberEight()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val consent = ConsentOptions.withConsentToAll()
        NumberEight.start(
            BuildConfig.REPLACE_WITH_DEVELOPER_KEY, this, consent,
            object: AuthorizationChallengeHandler {
                override fun requestAuthorization(context: Context,
                                                  permissions: Array<String>,
                                                  resolver: AuthorizationChallengeHandler.Resolver) {
                    pendingAuthChallenge = resolver
                    ActivityCompat.requestPermissions(this@MainActivity,
                        permissions,
                        REQUEST_NE_PERMISSIONS)
                }
            }, object: NumberEight.OnStartListener {
                override fun onSuccess() {
                    Log.i(LOG_TAG, "NumberEight started successfully.")
                }

                override fun onFailure(ex: Exception) {
                    Log.e(LOG_TAG, "NumberEight failed to start.", ex)
                }
            })
    }

    override fun onResume() {
        super.onResume()

        ne.onActivityUpdated { glimpse ->
            val activity = glimpse.mostProbable

            val text: Int
            val icon: Int
            when (activity.state) {
                NEActivity.State.Unknown -> {
                    text = R.string.activity_placeholder
                    icon = 0
                }
                NEActivity.State.Stationary -> {
                    text = R.string.stationary
                    icon = R.drawable.ic_stationary
                }
                NEActivity.State.Walking -> {
                    text = R.string.walking
                    icon = R.drawable.ic_walking
                }
                NEActivity.State.Running -> {
                    text = R.string.running
                    icon = R.drawable.ic_running
                }
                NEActivity.State.Cycling -> {
                    text = R.string.cycling
                    icon = R.drawable.ic_cycling
                }
                NEActivity.State.InVehicle -> {
                    text = R.string.in_vehicle
                    icon = R.drawable.ic_car
                }
            }

            runOnUiThread {
                // Set the text and icon
                findViewById<TextView>(R.id.activity_name).setText(text)
                findViewById<ImageView>(R.id.activity_image).setImageResource(icon)

                // Hide or show the loading bar
                val progressBar = findViewById<ProgressBar>(R.id.loading_bar)
                if (activity.state == NEActivity.State.Unknown) {
                    progressBar.visibility = View.VISIBLE
                } else {
                    progressBar.visibility = View.GONE
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        ne.unsubscribeFromAll()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            REQUEST_NE_PERMISSIONS -> {
                pendingAuthChallenge?.resolve()
            }
        }
    }

}
