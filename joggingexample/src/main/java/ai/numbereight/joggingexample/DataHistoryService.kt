package ai.numbereight.joggingexample

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import androidx.core.app.NotificationCompat
import android.util.Log
import io.realm.Realm
import ai.numbereight.sdk.NumberEight
import io.realm.RealmObject
import ai.numbereight.sdk.types.NEActivity
import android.os.Build
import java.util.*

open class JoggingSession(var start: Date, var end: Date? = null) : RealmObject() {
    constructor() : this(Date())

    val duration
    get() = end?.let { end ->
        end.time - start.time
    }
}

class DataHistoryService : Service() {
    companion object {
        const val LOG_TAG = "DataHistoryService"
        var IS_SERVICE_RUNNING = false
    }

    class ACTION {
        companion object {
            const val START_FOREGROUND = "ai.numbereight.recommendationexample.action.startforeground"
            const val STOP_FOREGROUND = "ai.numbereight.recommendationexample.action.stopforeground"
        }
    }

    class NOTIFICATION {
        companion object {
            const val NOTIFICATION_ID = 1
            const val CHANNEL_ID = "ai.numbereight.recommendationexample.recordingnotification"
        }
    }

    private val ne = NumberEight()
    private var current: JoggingSession? = null
    private var media: MediaPlayer? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            ACTION.START_FOREGROUND -> {
                start()
            }
            ACTION.STOP_FOREGROUND -> {
                stop()
            }
        }
        return Service.START_REDELIVER_INTENT
    }

    private fun start() {
        if (!IS_SERVICE_RUNNING) {
            val notification = makeNotification()
            startForeground(NOTIFICATION.NOTIFICATION_ID, notification)

            // Start audio track to act as a wakelock
            media = MediaPlayer.create(this, R.raw.wakeloop).run {
                isLooping = true
                setVolume(0f, 0f)
                start()
                this
            }

            // Subscribe to updates
            ne.onJoggingUpdated { glimpse ->
                val activity = glimpse.mostProbable
                when (activity.state) {
                    NEActivity.State.Running -> {
                        if (current == null) {
                            // Running started
                            current = JoggingSession(Date()).also {
                                Log.i(LOG_TAG, "Jogging session started at ${it.start}")
                            }
                        }
                    }
                    else -> {
                        current?.let {
                            // Running ended
                            it.end = Date()

                            Realm.getDefaultInstance().use { realm ->
                                realm.beginTransaction()
                                realm.copyToRealm(it)
                                realm.commitTransaction()
                            }

                            current = null

                            Log.i(LOG_TAG, "Jogging session ended at ${it.end}")
                        }
                    }
                }
            }

            Log.i(LOG_TAG, "Listening for workout events")

            IS_SERVICE_RUNNING = true
        }
    }

    private fun stop() {
        if (IS_SERVICE_RUNNING) {
            media?.stop()
            ne.unsubscribeFromJogging()
            stopForeground(true)
            Log.i(LOG_TAG, "Stopped listening for workout events")

            IS_SERVICE_RUNNING = false
        }
    }

    private fun makeNotification(): Notification {
        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP

        val mutabilityFlag = if (Build.VERSION.SDK_INT >= 23) {
            PendingIntent.FLAG_IMMUTABLE
        } else {
            0
        }

        val pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT + mutabilityFlag)

        return NotificationCompat.Builder(this, NOTIFICATION.CHANNEL_ID)
                .setContentTitle("Listening to workout events")
                .setTicker("Listening to workout events")
                .setSmallIcon(R.drawable.ic_running_white)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build()
    }

    override fun onBind(intent: Intent?): IBinder {
        return object: Binder() {}
    }
}