package ai.numbereight.joggingexample

import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.types.event.Glimpse
import ai.numbereight.sdk.types.NEActivity

const val JOGGING = "extended/jogging"

fun NumberEight.onJoggingUpdated(callback: (glimpse: Glimpse<NEActivity>) -> Unit): NumberEight {
    if (!subscriptions.containsKey(JOGGING)) {
        subscriptions[JOGGING] = mutableListOf()
    }

    subscriptions[JOGGING]!!.add(NumberEight.engine.subscribe(JOGGING, "type:NEActivity") { topic, event ->
        val values = event.values().map {
            Glimpse.ValuePair(it.value as NEActivity, it.confidence)
        }
        callback(Glimpse(values, topic))
    })
    return this
}

fun NumberEight.unsubscribeFromJogging() {
    subscriptions[JOGGING]?.run {
        forEach { it.cancel() }
        clear()
    }
}