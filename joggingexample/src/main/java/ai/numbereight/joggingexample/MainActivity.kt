package ai.numbereight.joggingexample

import com.google.android.material.bottomnavigation.BottomNavigationView
import android.content.Intent
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView

import java.lang.RuntimeException
import java.util.*
import kotlin.math.max

import ai.numbereight.sdk.NumberEight
import ai.numbereight.sdk.types.NEActivity


class MainActivity : AppCompatActivity() {
    companion object {
        const val MAX_GRAPH_SAMPLES = 20
        const val GRAPH_SCALE = 1f
    }

    private val ne = NumberEight()

    // Graph stuff
    private var graphFG: Int = 0
    private var graphBG: Int = 0
    private lateinit var graphHolder: SurfaceHolder
    private val graphData: Queue<Double> = LinkedList<Double>()

    private var pPrevious: Float = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Start the data service if not already started
        Intent(this, DataHistoryService::class.java).also { intent ->
            intent.action = DataHistoryService.ACTION.START_FOREGROUND
            startService(intent)
        }

        // Register navigation handlers
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_nav)
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menu_history -> {
                    val intent = Intent(this, HistoryActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }

            true
        }

        // Create the graph
        graphFG = getGraphColor(R.color.colorGraphForeground)
        graphBG = getGraphColor(R.color.colorGraphBackground)
        graphHolder = findViewById<SurfaceView>(R.id.graph).holder
        drawGraph()

        if (savedInstanceState != null) {
            savedInstanceState.getDoubleArray("graphData")?.let { data ->
                graphData.addAll(data.asIterable())
            }
        }

        // Subscribe to changes
        ne.onJoggingUpdated { glimpse ->
            var pJogging = glimpse.possibilities.find {
                it.value.state == NEActivity.State.Running
            }?.confidence?.toFloat() ?: 0f

            // Transform probability for the display
            pJogging = if (pJogging < 0.5) {
                pJogging * 4f / 3f
            } else {
                pJogging * (2f / 3f) + (1f / 3f)
            }

            val text = when {
                pJogging < 0.33 -> R.string.not_running
                pJogging < 0.66 -> R.string.maybe_running
                else -> R.string.running
            }

            runOnUiThread {
                // Set the text
                findViewById<TextView>(R.id.activity_name).setText(text)

                // Update the dial
                val from = (pPrevious - 0.5f) * 180f
                val to = (pJogging - 0.5f) * 180f
                val rotation = RotateAnimation(from, to,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.808f)
                rotation.duration = 1000
                rotation.fillAfter = true

                findViewById<ImageView>(R.id.indicator_view).startAnimation(rotation)

                pPrevious = pJogging
            }
        }

        ne.onActivityUpdated { glimpse ->
            val pRunning = glimpse.possibilities.find {
                it.value.state == NEActivity.State.Running
            }?.confidence ?: 0.0

            runOnUiThread {
                while (graphData.size >= MAX_GRAPH_SAMPLES) {
                    graphData.poll()
                }
                graphData.add(max(pRunning, 0.07))
                drawGraph()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        ne.unsubscribeFromJogging()
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putDoubleArray("graphData", graphData.toDoubleArray())
    }

    private fun getGraphColor(color: Int): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getColor(color)
        } else {
            @Suppress("DEPRECATION")
            resources.getColor(color)
        }
    }

    private fun drawGraph() {
        if (graphHolder.surface?.isValid != true) {
            return
        }

        val canvas = try {
            graphHolder.lockCanvas()
        } catch (_: RuntimeException) {
            return
        }

        if (canvas != null) {
            try {
                // Clear the canvas
                canvas.drawColor(graphBG)

                // Set the bar colour
                val paint = Paint()
                paint.style = Paint.Style.FILL
                paint.color = graphFG

                // Set the drawing scale
                canvas.save()
                canvas.scale(canvas.width / MAX_GRAPH_SAMPLES.toFloat(), -canvas.height / GRAPH_SCALE)

                // Move the drawing area to the far-right
                val offset = MAX_GRAPH_SAMPLES - graphData.size
                canvas.translate(offset.toFloat(), -GRAPH_SCALE)

                for (value in graphData) {
                    // Draw a column for this data point
                    canvas.drawRect(0f, value.toFloat(), 1.0f, 0f, paint)
                    canvas.translate(1f, 0f)
                }

                // Restore the canvas, ready for the next draw
                canvas.restore()
            } finally {
                graphHolder.unlockCanvasAndPost(canvas)
            }
        }
    }

}
